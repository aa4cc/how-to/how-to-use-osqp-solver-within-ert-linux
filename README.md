# How to use OSQP solver within ERT linux

**This tutorial is not completed yet (March 2023)**

This repository contains minimal working example for using OSQP within the ERT linux package.
Additionally, there is an example, how the OSQP can be used direcly for MPC control.


- The folder "OSQP_solver" contains Matlab system object for "Matlab System" block for Simulink.
- The folder "OSQP_thehood" contains several matlab scripts that build MPC controller using the OSQP.


# Installation
- If you want to use the OSQP solver directly in Matlab (not compiling in Simulink), e.g., running simulations:
    - Run "./OSQP_thehood/install_osqp.m". The OSQP should be intalled without any problems. See https://osqp.org/ in case of any problems


# Compiling OSQP for your target system
This step shows how to compile the OSQP C library from the source for the ERT linux - Matlab/Simulink 
However, we do not want to compile the library for our linux computer but direcly for the target (e.g., RPi) system.
There are two options: compile the OSQP on the target device or modify the Makefiles, so the OSQP is compiled for the taget device.
Here, only the simpler (recommended) option is shown:
    - On your **target** device, follow the instructions at https://osqp.org/docs/get_started/sources.html 
    - In the directory, **/osqp/build/out**, you should find the compiled static library **libosqp.a**
    - Take this file, and copy it into the **host** computer.


# Compiling ERT linux wrapper for OSQP solver in Simulink
- First, make sure, that you followed the instruction at 
    - https://gitlab.fel.cvut.cz/aa4cc/how-to/how-to-use-ert-linux-package 
    and the cross-compilation works.
- 


# First example 
- The simplest example is **demo_OSQP.slx** with corresponding **demo_simple_osqp.m**.


# Known issues:
When compiling the simulink diagram, a following error can pop-out:
    - The current "ExtModeMexFile" value, "ext_comm", does not match "noextcomm", which is the value expected by the current external mode transport layer, "none". This transport layer is specified by the 'ExtModeTransport' value, "0". This error can occur when you change the way you register external mode transport layers in Simulink. Refresh "ExtModeMexFile" by reselecting the required external mode transport layer.

Usually, restart of the Matlab helps.