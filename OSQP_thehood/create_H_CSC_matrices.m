function [H_x, H_i,H_p] = create_H_CSC_matrices(H)
% Compressed Sparse Column of upper triangular part of the matrix H

nV = size(H,1);

% Values of H_x are ordered by increasing Column index of the entriex of H
H_x = zeros(1, sum(1:nV));
idx = 1;
for jj = 1:nV
    for ii = 1:nV
        if (jj >= ii)
            H_x(idx) = H(jj,ii);
            idx = idx + 1; 
        end 
    end
end

% Row indeces: H_i[idx] holds the row index of the entry H_x[idx]
H_i = [0];
for ii = 1:nV-1
    H_i = [H_i, 0:1:ii];
end

% H_p = 0:1:nV-1;
H_p = [0];
for ii = 1:nV-1
    H_p = [H_p, H_p(ii) + ii];
end

H_p = [H_p,  sum(1:nV)];


end