%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%
clear;
clc;
%%
% This script setups the OSQP solver for ERT linux.
% In particular, it links the compiled (static) libraries that are necessary to run the C/C++ wrapper for OSQP 
% Notes:
%   - the libraries for OSQP need to be manually put into the folders
%   - When another libraries are used

path_to_libraries = '';

system_lib = ["SYSTEM_LIBS += -lm -lrt -lpthread -losqp -ldl -L/home/loido/RPI_libs"];

% Define problem data
H = [4, 1; 1, 2];
A = [1, 1; 1, 0];
lb = [1; 0];
ub = [1; 0.7];
g = [1; 1];

% Create CSC sparse matrix representation
[H_x, H_i,H_p] = create_H_CSC_matrices(H);
[A_x, A_i,A_p] = create_H_CSC_matrices(A);

