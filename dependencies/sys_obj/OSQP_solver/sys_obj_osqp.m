classdef sys_obj_osqp < matlab.System & coder.ExternalDependency ...
        % This Matlab object is responsible for:

    properties
        % Simulink !! does not !! allow parameters whose values are booleans, literal character
        % vectors, string objects or non-numeric structures to be tunable.

        % Do not put any fields here (maybe).
    end

    % Public, non-tunable properties
    properties(Nontunable)
        nV = 2; % nV: Num of decision variables
        nC = 3; % nC: Num of constraints
    end

    % Properties that are allowed to change their values during simulation.
    % Use to store internal values
    properties(Access = private)
        % E.g.:
    end

    methods
        % -----------------------------------
        % ---------Constructor---------------
        % -----------------------------------
        function obj = sys_obj_osqp(varargin)
            setProperties(obj,nargin,varargin{:}); % Set the properties based on the input in the 'mask'. Override the default parameters
        end
        % -----------------------------------
        % -----------------------------------

    end

    methods(Access = protected)

        % -----------------------------------
        % ---------Setup---------------------
        % -----------------------------------
        function setupImpl(obj)
            % This if/else is necessary in setupImpl when C-function is called
            if isempty(coder.target)
                % Just do nothing?
            else
                coder.cinclude('osqp_ERT.h');
                coder.ceval('init');
            end
            % We could perform one-time calculations, such as computing constants
        end


        % -----------------------------------
        % ---------Step----------------------
        % -----------------------------------
        function [opt_torque] = stepImpl(obj,   H, H_i, H_p, ...
                                                A_x, A_i, A_p, ...
                                                g, lb, ub)
            nV = numel(lb);
            nC = numel(lb); % TODO: add as another input? This might not be correct
            %%%%%%%%%%%%%%%%%%
            opt_torque = 0; % Pre-alocate memory for C function; We want only input for next step
            % coder.ceval('step_v2', H(:), g(:), lb(:), ub(:), obj.nV, coder.wref(opt_torque));
            %%%%%%%%%%%%%%%%%%
            H_nnz = numel(H);

            g_size = numel(g); 

            A_nnz  = numel(A_x);

            len_lb = numel(lb);

            len_ub = numel(ub); 

            opt_torque = 0;

            coder.ceval('step', H, H_i, H_p, H_nnz, ...
                                g, g_size, ...
                                A_x, A_i, A_p, A_nnz, ...
                                lb, len_lb, ...
                                ub, len_ub, ...
                                obj.nV, obj.nC, coder.wref(opt_torque));
        end


        % -------------------------------------------------------------
        % ---------Function to be called at the end of program---------
        % -------------------------------------------------------------
        function releaseImpl(obj)
            if isempty(coder.target)
                % Just do nothing?
            else
                % Call C-function implementing device termination
                % E.g.:
                coder.ceval('terminate');
            end
        end


        % ----------------------
        % INPUTS
        % ----------------------

        function num = getNumInputsImpl(~)
            num = 9;
        end

        function in = getInputNamesImpl(obj)
            in(1) = "H_x";
            in(2) = "H_i";
            in(3) = "H_p";

            in(4) = "A_x";
            in(5) = "A_i";
            in(6) = "A_p";

            in(7) = "g";
            in(8) = "lb";
            in(9) = "ub";
        end

        function in = getInputDataTypeImpl(obj)
            in(1) = 'double';   % H
            in(2) = 'int';      % H_i
            in(3) = 'int';      % H_p

            in(1) = 'double';   % A_x
            in(2) = 'int';      % A_i
            in(3) = 'int';      % A_p

            in(4) = 'double';   % g
            in(5) = 'double';   % lb
            in(6) = 'double';   % ub
        end

        function in = getInputSizeImpl(obj)
            in(1) = [1, sum(1:obj.nV)]; % H
            in(2) = [1, sum(1:obj.nV)]; % H_i
            in(3) = [1, obj.nV+1];       % H_p

            in(1) = [1,sum(1:obj.nV)]; % A_x
            in(2) = [1,sum(1:obj.nV)]; % A_i
            in(3) = [1,obj.nV+1];       % A_p

            in(4) = [1,obj.nV];         % g
            in(5) = [1,obj.nC];         % lb
            in(6) = [1,obj.nC];         % ub
        end


        % ----------------------
        % OUTPUTS
        % ----------------------

        function num = getNumOutputsImpl(obj)
            num = 1;
        end

        function out = getOutputSizeImpl(obj)
            out = 1;
        end

        function out = getOutputNamesImpl(obj)
            out(1) = "Torque";
        end


        % This function just needed to be here
        function out = isOutputSizeLockedImpl(~,~)
            out = true;
        end

        % ----------------------
        function out = getOutputDataTypeImpl(obj)
            out = 'double';
        end

        % ----------------------
        function out = isOutputFixedSizeImpl(obj,~)
            out = true;
        end

        % ----------------------
        function out = isOutputComplexImpl(obj)
            out = false;
        end

        function icon = getIconImpl(obj)
            icon = ["Test", "OSQP"]; % Example: text icon
            % icon = ["My","System"]; % Example: multi-line text icon
            % icon = matlab.system.display.Icon("myicon.jpg"); % Example: image file icon
        end

    end




    methods (Static, Access=protected)
        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end

        function isVisible = showSimulateUsingImpl
            isVisible = false;
        end
    end



    methods(Static)
        function name = getDescriptiveName()
            name = 'SMT';
        end

        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end

        function updateBuildInfo(buildInfo, context)
            % IMPORTANT
            if context.isCodeGenTarget('rtw')
                srcDir = fullfile(fileparts(mfilename('fullpath')),'cpp_osqp');
                includeDir = fullfile(fileparts(mfilename('fullpath')),'cpp_osqp');
                %                 includeDir2 = fullfile(fileparts(mfilename('fullpath')),'qpOASES');

                addIncludePaths(buildInfo,includeDir);
                %                 addIncludePaths(buildInfo,includeDir2);

                addIncludeFiles(buildInfo,'osqp_ERT.h', includeDir);
                % addIncludeFiles(buildInfo,'qpOASES.h', includeDir);
                %                 addIncludeFiles(buildInfo,'QProblem.hpp', includeDir2);

                addSourceFiles(buildInfo,'osqp_ERT.c', srcDir);

                addCompileFlags(buildInfo,{'-std=c++14'});
            end
        end

    end
end