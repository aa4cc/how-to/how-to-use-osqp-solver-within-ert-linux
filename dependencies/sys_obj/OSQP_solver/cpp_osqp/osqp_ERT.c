#include "osqp.h"
#include "osqp_ERT.h"



// Workspace structures
OSQPWorkspace *work;
OSQPSettings  *settings;
OSQPData      *data;     
int init_F = 1;

void copyFloatArray(c_float *ret_arr, real_T in_arr[], int length) {
    for (int i = 0; i < length; i++) {
        ret_arr[i] = in_arr[i];
    } 
}

void copyIntArray(c_int *ret_arr, real_T in_arr[], int length) {
    for (int i = 0; i < length; i++) {
        ret_arr[i] = in_arr[i];
    } 
}


void init() {
    settings = (OSQPSettings *)c_malloc(sizeof(OSQPSettings));
    data = (OSQPData *)c_malloc(sizeof(OSQPData));
}


void step(  real_T iP_x[], real_T iP_i[], real_T iP_p[], int len_P, 
            real_T iq[], int len_q,
            real_T iA_x[], real_T iA_i[], real_T iA_p[], int len_A,
            real_T il[], int len_l, 
            real_T iu[], int len_u,
            int nV, int nC, 
            real_T* torque_out) {
    // Solve: 
    //       min    (x'Hx + g'x)
    //       s.t.   lb <= Ax <= ub
    // The other matrices define the CSC formate of sparse matrices
    // printf("Init flag is %d \n", init_F);   

    // Define solver settings as default
    if (settings) osqp_set_default_settings(settings);

    // -------------------------------
    // Define number of decision variables and number of constraints
    c_int n = nV;
    c_int m = nC;
    c_int exitflag = 0;

    // -------------------------------
    // Matrix q (gradient)
    c_float q[len_q];  
    copyFloatArray(q, iq, len_q);
    // --------

    if (init_F == 1) {
        // -------------------------------
        // Matrix P (Hessian)
        c_float P_x[len_P];
        c_int   P_i[len_P];
        c_int   P_p[len_P+1];
        c_int   P_nnz  = len_P;
        copyFloatArray(P_x, iP_x, len_P);
        copyIntArray(P_i, iP_i, len_P);
        copyIntArray(P_p, iP_p, len_P);
        // --------
        // -------------------------------
        // Matrix A (Constraints)
        c_float A_x[len_A];
        c_int   A_i[len_A];
        c_int   A_p[nC+1];                             
        c_int   A_nnz = len_A;
        copyFloatArray(A_x, iA_x, len_A);
        copyIntArray(A_i, iA_i, len_A);
        copyIntArray(A_p, iA_p, nC+1);
        // --------
        // -------------------------------
        // Lower bounds (Constraints)
        c_float l[len_l];
        copyFloatArray(l, il, len_l);
        // --------
        // -------------------------------
        // Upper bounds (Constraints)
        c_float u[len_u];
        copyFloatArray(u, iu, len_u);
        // --------

        // Populate data
        if (data) {
            data->n = n;
            data->m = m;
            data->P = csc_matrix(data->n, data->n, P_nnz, P_x, P_i, P_p);
            data->q = q;
            data->A = csc_matrix(data->m, data->n, A_nnz, A_x, A_i, A_p);
            data->l = l;
            data->u = u;
        }

        // Setup workspace
        exitflag = osqp_setup(&work, data, settings);   
        init_F = 0;
        // Solve problem
        osqp_solve(work);
        osqp_update_verbose(work, 0); // deactivate verbose
    } else {
        // Update problem
        osqp_update_lin_cost(work, q);
        // Solve problem
        osqp_solve(work);
    }
    

 
    *torque_out = work->solution->x[0];  // <------------------ TODO: return all decision variables

    // printf("The solution is [%f , %f ]\n", work->solution->x[0], work->solution->x[1]);   
    fflush(stdout);
}

// void step() {
//     c_float q_new[2] = {2.0, 3.0, };
//     c_float l_new[3] = {2.0, -1.0, -1.0, };
//     c_float u_new[3] = {2.0, 2.5, 2.5, };

//     // Update problem
//     osqp_update_lin_cost(work, q_new);
//     osqp_update_bounds(work, l_new, u_new);

//     // Solve updated problem
//     osqp_solve(work);
//     fflush(stdout);
// }


void terminate() {
    osqp_cleanup(work);
    if (data) {
        if (data->A) c_free(data->A);
        if (data->P) c_free(data->P);
        c_free(data);
    }
    if (settings) c_free(settings);
}