

#ifdef __cplusplus
extern "C" {
#endif


#include "rtwtypes.h"


void step(  real_T iP_x[], real_T iP_i[], real_T iP_p[], int len_P, 
            real_T iq[], int len_q,
            real_T iA[], real_T iA_i[], real_T iA_p[], int len_A,
            real_T l[], int len_l, 
            real_T u[], int len_u,
            int nV, int nC,
            real_T* torque_out);

void init();

void terminate();


#ifdef __cplusplus
}
#endif